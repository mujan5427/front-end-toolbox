// Imported Files
const express = require('express');
const path    = require('path');

// Constant Variables
const PORT                  = 3000;
const STATIC_FILE_DIRECTORY = 'dist';

const server = express();


server.use(express.static(path.resolve(path.parse(__dirname).dir, `../${ STATIC_FILE_DIRECTORY }`)));

server.listen(PORT, function () {
  console.log(`Server listening on port ${ PORT }`);
});
