const path                 = require('path');
const webpack              = require('webpack');
const CopyWebpackPlugin    = require('copy-webpack-plugin');
const CleanWebpackPlugin   = require('clean-webpack-plugin');
const HtmlWebpackPlugin    = require('html-webpack-plugin');
const UglifyJsPlugin       = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


module.exports = {
  entry: {
    app: path.resolve(__dirname, '../src/js/app.js')
  },
  output: {
    filename: '[name].js',
    sourceMapFilename: '[file].map',
    path: path.resolve(__dirname, '../dist'),

    // 設定靜態資源的 root path
    // `html-webpack-plugin` 被注入的 chunk 檔案的路徑，會根據此 property
    publicPath: './'
  },
  mode: process.env.NODE_ENV || 'production',
  devtool: 'source-map',
  module: {
    rules: [
      { test: /\.js$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
          'eslint-loader',
        ]
      },
      {
        test: /\.(png|jpg|gif|svg)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name: '[sha512:hash:base64:7].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader'
        }
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin(
      [path.resolve(path.parse(__dirname).dir, 'dist')],
      { root: path.parse(__dirname).dir }
    ),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, '../src/css/utility/_media-query.scss'),
        to: path.resolve(__dirname, '../dist/media-query.scss')
      },
      {
        from: path.resolve(__dirname, '../src/css/variable/_variable.scss'),
        to: path.resolve(__dirname, '../dist/variable.scss')
      }
    ]),
    new HtmlWebpackPlugin({
      filename: 'index.html',

      // 設定 HTML 模板
      template: path.resolve(__dirname, '../src/html/index.html'),
      inject: 'body',
      chunks: ['app'],
      chunksSortMode: 'manual',

      // minify HTML
      minify: {
        removeComments: true,
        removeAttributeQuotes: true,
        collapseWhitespace: true
      }
    }),
    new UglifyJsPlugin({
      uglifyOptions: {
        warnings: false,
        compress: true,
        output: {
          comments: false
        }
      },
      sourceMap: true
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css'
    })
  ]
};
