module.exports = {
  plugins: [
    require('autoprefixer')({
      browsers: ['last 2 versions']
    }),
    require('cssnano')({
      discardComments: {
        removeAll: true
      },
      discardUnused: false,
      mergeIdents: false,
      zindex: false
    })
  ]
};
